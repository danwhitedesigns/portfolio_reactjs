import React from 'react';
import { Link } from 'react-router-dom';

class Nav extends React.Component {
    render() {
        return (
            <div className="collapse navbar-collapse justify-content-center" id="navbarCollapse">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link to="/" className="nav-link">
                            Home
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="About" className="nav-link">
                            About
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="Contact" className="nav-link">
                            Contact
                        </Link>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Nav;
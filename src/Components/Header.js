import React from 'react'
import Nav from './Nav';

class Header extends React.Component {

    render() {
        return (
            <header className="header pt-4">
                <div className="content">
                    <div className="text-center">
                        <h1 className="m-0">{this.props.siteTitle}</h1>
                    </div>
                    <nav className="navbar navbar-expand-md">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon" />
                        </button>
                        <Nav />
                    </nav>
                </div>
            </header>
        );
    }
}



export default Header
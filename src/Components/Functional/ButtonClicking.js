//We are using LocalStorage with this component so lets import it
import localStorage from 'local-storage'
import React from 'react'

class ButtonClicking extends React.Component {
    //Here is our constructor
    constructor() {
        //super() is needed to call 'this.' and if the component has a constructor
        super();
        //see we can use 'this.', state is an obj and have a property called count
        const count = (localStorage.get('count') === null) ? 0 : localStorage.get('count');
        this.state = {
            count: count,
        };
    }

    //Declaring the updateCount function that will set the state value of count to its previous value + 1;
    updateCount() {
        this.setState((prevState) => {
            let updatedValue = prevState.count + 1;
            localStorage.set('count', updatedValue);
            return { count: updatedValue }
        });
    }

    //Render function, this is what is rendered to the DOM
    render() {
        //We return a button element with an onClick event calling the updateCount() function. 
        //Then we render the value of the state count which will update each time you click the button
        return (
            <div>
                <div className="mt-3 mb-3">
                    <button onClick={() => this.updateCount()}>
                        Clicked {this.state.count} times
                    </button>
                </div>
                <div>
                    <p>The button was clicked {this.state.count} times</p>
                </div>
            </div>
        );
    }
}
export default ButtonClicking
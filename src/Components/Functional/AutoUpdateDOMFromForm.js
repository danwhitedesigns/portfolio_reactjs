import React from 'react'
import localStorage from 'local-storage'
import $ from 'jquery';

class AutoUpdateDOMFromForm extends React.Component {
    constructor() {
        super();
        const savedInput = (localStorage.get('savedInput') === null) ? "" : localStorage.get('savedInput');
        this.state = { input: '', savedInput: savedInput };
    }

    autoUpdateText = (event) => {
        this.setState({ input: event.target.value });
    }

    saveInput() {
        if ($('input[name=inputField').val()) {
            this.setState((prevState, props) => {
                let updatedInputValue = prevState.savedInput + " " + this.state.input;
                localStorage.set('savedInput', updatedInputValue);
                $('input[name=inputField').val('');
                this.setState({ input: "" });
                $(".save-alert").fadeIn();
                setTimeout(function () { $(".save-alert").fadeOut(); }, 1500);
                return { savedInput: updatedInputValue }
            });
        }
    }
    resetAllInput() {
        if (this.state.savedInput) {
            this.setState((prevState, props) => {
                let updatedInputValue = "";
                localStorage.set('savedInput', updatedInputValue);
                $('input[name=inputField').val('');
                this.setState({ input: "" });
                $(".reset-alert").fadeIn();
                setTimeout(function () { $(".reset-alert").fadeOut(); }, 1500);
                return { savedInput: updatedInputValue }
            });
        }
    }

    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <div className="row">
                        <div className="col-12">
                            <p>Enter some text below and watch it auto update below</p>
                            <input type='text' name="inputField" placeholder="Enter some text" value={this.state.resetValue} onChange={this.autoUpdateText} />
                            <button className="ml-2" onClick={() => this.saveInput()}>Save</button>
                            <button className="ml-2" onClick={() => this.resetAllInput()}>Reset</button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <p className="text-left p-2">{this.state.savedInput} {this.state.input}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AutoUpdateDOMFromForm
import Pages from '../Pages/Pages';
const appMetaTitle = "| ReactJS Portfolio"
const defaultMetaDescription = "This is a generic description";
const defaultMetaKeywords = "These, are, generic, keywords";
const appTitle = "Dan White";


export default { appMetaTitle, appTitle, Pages, defaultMetaDescription, defaultMetaKeywords };
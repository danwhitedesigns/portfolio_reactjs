import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './Components/Header';
//Settings
import * as settings from "./Settings/Settings";

class AppContent extends React.Component {

    render() {
        return (

            <Router>
                <Header siteTitle={settings.default.appTitle} />
                <main>
                    <Switch>
                        <Route path={settings.default.Pages.homePath} exact component={settings.default.Pages.Home} />
                        <Route path={settings.default.Pages.aboutPath} component={settings.default.Pages.About} />
                        <Route path={settings.default.Pages.contactPath} component={settings.default.Pages.Contact} />
                    </Switch>
                </main>
            </Router>
        );
    }
}

export default AppContent;

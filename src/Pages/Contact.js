import React from 'react';
import MetaData from './PageMeta/Meta';
import * as settings from "../Settings/Settings"
import AutoUpdateDOMFromForm from '../Components/Functional/AutoUpdateDOMFromForm';
import ButtonClicking from '../Components/Functional/ButtonClicking';

class Contact extends React.Component {


    render() {
        var alertStyle = {
            display: 'none',
        };
        const pageTitle = "Contact";
        const pageDesc = settings.default.defaultMetaDescription;
        const pageKeywords = settings.default.defaultMetaKeywords;

        return (
            <>
                <MetaData pageTitle={pageTitle} pageDesc={pageDesc} pageKeywords={pageKeywords} />
                <main>
                    <div className="Contact">
                        <div className="row mt-3 mb-3 mx-auto">
                            <div className="col-12">
                                <h1 className="text-center">{pageTitle}</h1>
                            </div>
                        </div>
                    </div>
                    <div className="container pt-5 pb-5">
                        <div className="row">
                            <div className="col-12 text-center">
                                <AutoUpdateDOMFromForm />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12 text-center">
                                <ButtonClicking />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12">
                                <div className="alert alert-success save-alert" style={alertStyle} role="alert">
                                    Text Saved
                            </div>
                                <div className="alert alert-danger reset-alert" style={alertStyle} role="alert">
                                    Text Reset
                            </div>
                            </div>
                        </div>
                    </div>
                </main>
            </>
        );
    }
}

export default Contact;
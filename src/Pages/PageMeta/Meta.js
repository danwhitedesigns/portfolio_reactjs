import React from 'react';
import Helmet from 'react-helmet';
import * as settings from "../../Settings/Settings";

class Meta extends React.Component {
    render() {
        return (
                <Helmet>
                    <title>{this.props.pageTitle + " " + settings.default.appMetaTitle}</title>
                    <meta name="description" content={this.props.pageDesc} />
                    <meta name="keywords" content={this.props.pageKeywords} />
                </Helmet>
        );
    }
}

export default Meta;
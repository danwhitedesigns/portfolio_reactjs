//First import all page components
import Home from './Home';
import About from './About';
import Contact from './Contact';

//Now set paths to page content
const homePath = "/";
const aboutPath = "/about";
const contactPath = "/contact";


export default { Home, homePath, About, aboutPath, Contact, contactPath };
import React from 'react';
import PwaInstallPopupIOS from 'react-pwa-install-ios';
//AppContent
import AppContent from './AppContent';
//Stylesheet
import './Styles/Compiled/style.css';
class App extends React.Component {

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-12">
                        <AppContent />

                        <PwaInstallPopupIOS>
                            <div delay={1} className="text-center p-2">
                                <h2 className="m-0">
                                    Would you like to install this app on your device?
                        </h2>
                                <h3 className="m-0">
                                    Use the <i className="bi bi-box-arrow-in-down"></i> icon in your browser, then tab 'Add to Home Screen'.
                        </h3>
                            </div>
                        </PwaInstallPopupIOS>
                    </div>
                </div>
            </div>

        );
    }
}

export default App;

# README #

### Creating a new page ###

* Create new js file under src/Pages
* Populate with content to render for that page and export it
* Import the new page in Pages.js and create a const for its path
* Add the new page as a new Route in AppContent.js
* Finally add a link to the page in the Nav or wherever you want to link to the page

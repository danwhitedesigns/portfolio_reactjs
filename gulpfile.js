const gulp = require('gulp');
const sass = require('gulp-sass');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');

var stylesPath = 'src/Styles';
var stylesCompiledPath = 'src/Styles/Compiled';


function compile() {
    return gulp.src(stylesPath + '/Scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest(stylesCompiledPath));
}

function watch() {
    gulp.watch(stylesPath + '/Scss/**/*.scss', compile);
}


exports.compile = compile;
exports.watch = watch;